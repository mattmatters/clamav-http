# ClamAV REST Proxy

Proxy file scanning through a rest endpoint.

This abstracts a lot of messaging and handling of working with clam behind
an endpoint. So now you only need to stream a file to endpoint.

This also gives back a standardized set up messages.

It's fast too!

## Building

```sh
docker build .
```

## Running in dev

```sh
docker-compose up
```
