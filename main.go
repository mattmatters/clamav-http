package main

import (
	"flag"
	"github.com/dutchcoders/go-clamd"
)

const urlHelpTxt = "The location of the clamd port"

func main() {
	location := flag.String("url", "/tmp/clamd.socket", urlHelpTxt)
	flag.Parse()
	clamScan := clamd.NewClamd(*location)
	serve(clamScan)
}
