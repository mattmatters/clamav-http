#!/bin/bash
set -m

host=${CLAMD_HOST:-192.168.50.72}
port=${CLAMD_PORT:-3310}

echo "using clamd server: $host:$port"

sleep 20s

echo "starting now"
app --url="tcp://${host}:${port}" serve
echo "finished"
