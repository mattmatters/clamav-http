package main

import (
	"encoding/json"
	"github.com/dutchcoders/go-clamd"
	"io"
	"log"
	"net/http"
)

// We wrap this so we can pass the scanner around to handlers
type server struct {
	scanner *clamd.Clamd
}

type scanResult struct {
	Safe  bool     `json:"safe"`  // Quick answer for if the file is safe
	Found []string `json:"found"` // List of bad stuff found
}

func (s *server) handleStream(w http.ResponseWriter, r *http.Request) {
	res, err := s.ScanStream(r.Body)

	if err != nil {
		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad file"))
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(res)
	}
}

// Wrapper around scanning a stream of bytes
func (s *server) ScanStream(data io.Reader) (scanResult, error) {
	response, err := s.scanner.ScanStream(data, make(chan bool))

	return fmtClamRes(response), err
}

func fmtClamRes(res chan *clamd.ScanResult) scanResult {
	foundRes := scanResult{Safe: true, Found: []string{}}

	for s := range res {
		if s.Status == "FOUND" {
			foundRes.Safe = false
			foundRes.Found = append(foundRes.Found, s.Description)
		}
	}

	return foundRes
}

func serve(scanner *clamd.Clamd) {
	scanServe := server{scanner: scanner}
	http.HandleFunc("/stream", scanServe.handleStream)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
